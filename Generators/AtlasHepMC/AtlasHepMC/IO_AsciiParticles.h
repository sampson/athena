/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_IOASCIIPARTICLES_H
#define ATLASHEPMC_IOASCIIPARTICLES_H
#include "HepMC/IO_AsciiParticles.h"
#endif
